#include <iostream>
#include "mylib.h"
#include <stdio.h>

#include <stdlib.h>
#include <ctime>
#include <cmath>
#include <fstream>
#include <locale>
#include <string>
#include <filesystem>
#include <cstdlib>

#define MY_RANGE(cin_num, range) ((cin_num >= 0 && cin_num < range))

#define Bubble(a, b)a = a^b;b = b ^ a;a = a ^ b;

#pragma pack(push, 1)
struct Form
{
	char SotFullName[100];   
	char SotAge[100];        
	short SotRang;           
	short SotSalary;           
	char SotPhone;           
};
#pragma pack(pop)


int main()
{

	//TASK_1 ============================

	float* p_arr = Task5::Init(5);
	Task5::Count(p_arr, 5);
	Task5::Print(p_arr, 5);
	std::cout <<"\n\n";

    //Task2 =============================================

	int number;
	const int MAX_RANGE = 10;
	std::cout << "Input number: " << std::endl;
	std::cin >> number;
	bool rezult = MY_RANGE(number, MAX_RANGE);
	std::cout << "Your number " << (rezult ? "= true" : "= false") << "\n\n";

	//Task 3 =========================

	int* b;
	int a;
	std::cout << "Input number: ";
	std::cin >> a;
	b = new int[a];

	for (int i = 0; i < a; i++)
	{
		std::cout << "a[" << i << "] = ";
		std::cin >> b[i];
	}
	int time; 
	
	for (int i = 0; i < a - 1; i++)
	{
		for (int j = 0; j < a - i - 1; j++)
		{
			if (b[j] > b[j + 1])
			{		
				Bubble(b[j], b[j + 1]);
				
			}
		}
	}	
	   for (int i = 0; i < a; i++) 
	   {
		std::cout << b[i] << " ";
	   }
	   std::cout << std::endl;
	   
	delete[] b; 

	std::cout << "\n";


   //Task 4 ==================================================

	auto* Personal = new Form;	
	std::cout << "Size = " << sizeof(Form) << " byte" << std::endl;
	std::cout << "File create" << "\n";
	std::string path = "Task_4.txt";
	std::ofstream fout;
	fout.open(path);
	if (!fout.is_open())
	{
		std::cout << "Error file open!" << std::endl;
	}
	else
	{
		std::string Form;		
		fout.write((char*)Personal, sizeof(Personal));
	}
	fout.close();

	delete Personal;

	return 0;
}