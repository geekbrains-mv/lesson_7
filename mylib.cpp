#include <iostream>
#include <cstdlib>

namespace Task5
{
	//Task 1

 	float* Init(const int Size)
	{
		float* ptr = new float[Size];
		setlocale(LC_ALL, "rus");
		for (int i = 0; i < Size; i++) {
			ptr[i] = (i % 2 == 1 ? 1 : -1) * i;
		}
		return ptr;
	}

	void Count(float* p_arr, const int Size)
	{

        int Pos = 0, Neg = 0;
		for (int i = 0; i < Size; i++)
		{
			if (p_arr[i] < 0)
            {
                Pos++;
            }
			else
            {
                Neg++;
            }
				
		}
		std::cout << "Possitive count:  " << Pos << std::endl;
		std::cout << "Negative count:  " << Neg << std::endl;
	}

	void Print(float* p_arr, const int Size)
	{
		for (int x = 0; x < Size; x++)
        {
            std::cout << p_arr[x] << " ";
        }
			
	}

}
